package tn.esprit.projectmanagement.config;

import org.springframework.context.annotation.Bean;
import tn.esprit.projectmanagement.service.ScrumTeam;
import tn.esprit.projectmanagement.service.TeamMember;

public class BeansConfiguration {
    @Bean(name="dev-salma")
    public TeamMember getTeamMemberOne()
    {
        TeamMember ret = new TeamMember();
        ret.setName("Salma LANDOULSI");
        ret.setRole("dev");
        return ret;
    }
    @Bean(name="po-rihab")
    public TeamMember getTeamMemberTwo()
    {
        TeamMember ret = new TeamMember();
        ret.setName("Rihab IDOUDI");
        ret.setRole("po");
        return ret;
    }
    @Bean(name="scrum-team")
    public ScrumTeam getScrumTeam()
    {
        ScrumTeam st = new ScrumTeam();
        return st;
    }
}
