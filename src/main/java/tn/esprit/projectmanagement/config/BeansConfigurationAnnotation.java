package tn.esprit.projectmanagement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import tn.esprit.projectmanagement.service.ScrumTeamAnnotation;
import tn.esprit.projectmanagement.service.TeamMemberAnnotation;

@Configuration
@ComponentScan("tn.esprit.projectmanagement.service")
public class BeansConfigurationAnnotation {
    @Bean(name="dev-salma")
    public TeamMemberAnnotation getTeamMemberOne()
    {
        TeamMemberAnnotation ret = new TeamMemberAnnotation();
        ret.setName("Salma LANDOULSI");
        ret.setRole("dev");
        return ret;
    }
    @Bean(name="po-rihab")
    public TeamMemberAnnotation getTeamMemberTwo()
    {
        TeamMemberAnnotation ret = new TeamMemberAnnotation();
        ret.setName("Rihab IDOUDI");
        ret.setRole("po");
        return ret;
    }
    @Bean(name="scrum-team")
    public ScrumTeamAnnotation getScrumTeam()
    {
        ScrumTeamAnnotation st = new ScrumTeamAnnotation();
        return st;
    }
}
