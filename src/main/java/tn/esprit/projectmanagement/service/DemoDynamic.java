package tn.esprit.projectmanagement.service;

import java.io.File;
import java.util.Scanner;
import org.apache.log4j.Logger;

public class DemoDynamic { 
	private static final Logger logger = Logger.getLogger(DemoDynamic.class);
	// Injection de dépendances dynamique :  
	public void createScrumTeam () { 
		Scanner scanner = null; 
		try {
			logger.info("In createScrumTeam() : ");
			scanner = new Scanner(new File("config.txt")); 
			String memberClassName = scanner.nextLine(); 
			logger.debug("memberClassName : " + memberClassName);
			Class<?> cmember = Class.forName(memberClassName);
			logger.debug("cmember : " + cmember);
			ITeamMember member = (ITeamMember) cmember.newInstance(); 
			logger.debug(" name of team member : " + member.getMemberDetails()[1]);
			logger.info("Out of createScrumTeam(). ");
		} catch (Exception e) {logger.error("Error : " + e);} 
		finally { logger.info("dans le finally"); if (scanner!=null) scanner.close(); }
	}
}
 