package tn.esprit.projectmanagement.service;

public class TeamMember2 implements ITeamMember { 

	private String role = "developper"; 
	private String name = "THouraya LOUATI"; 
	private int nbYearExperence;

	public String getRole() {return role;}
	public void setRole(String role) {this.role = role;}
	public String getName() {return name;}
	public void setName(String name) {this.name = name; }
	public int getNbYearExperence() {return nbYearExperence;}
	public void setNbYearExperence(int nbYearExperence) {this.nbYearExperence = nbYearExperence;}
	
	@Override
	public String[] getMemberDetails() {
		return new String [] {role, name, Integer.toString(nbYearExperence)}; 
	}

}
