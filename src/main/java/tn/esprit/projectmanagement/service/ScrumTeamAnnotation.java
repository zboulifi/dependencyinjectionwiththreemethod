package tn.esprit.projectmanagement.service;

import org.springframework.stereotype.Component;

@Component
public class ScrumTeamAnnotation {


    private TeamMemberAnnotation productOwner;
    private TeamMemberAnnotation scrumMaster;
    private TeamMemberAnnotation developper;

    public TeamMemberAnnotation getProductOwner() {
        return productOwner;
    }
    public void setProductOwner(TeamMemberAnnotation productOwner) {
        this.productOwner = productOwner;
    }

    public TeamMemberAnnotation getScrumMaster() {
        return scrumMaster;
    }
    public void setScrumMaster(TeamMemberAnnotation scrumMaster) {
        this.scrumMaster = scrumMaster;
    }

    public TeamMemberAnnotation getDevelopper() {
        return developper;
    }
    public void setDevelopper(TeamMemberAnnotation developper) {
        this.developper = developper;
    }

}
