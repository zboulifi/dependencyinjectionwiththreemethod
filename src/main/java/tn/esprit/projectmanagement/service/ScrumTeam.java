package tn.esprit.projectmanagement.service;

import org.springframework.stereotype.Component;

public class ScrumTeam {

	private TeamMember productOwner;
	private TeamMember scrumMaster;
	private TeamMember developper;

	public TeamMember getProductOwner() {
		return productOwner;
	}
	public void setProductOwner(TeamMember productOwner) {
		this.productOwner = productOwner;
	}

	public TeamMember getScrumMaster() {
		return scrumMaster;
	}
	public void setScrumMaster(TeamMember scrumMaster) {
		this.scrumMaster = scrumMaster;
	}

	public TeamMember getDevelopper() {
		return developper;
	}
	public void setDevelopper(TeamMember developper) {
		this.developper = developper;
	}
	
}
