package tn.esprit.projectmanagement.service;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import tn.esprit.projectmanagement.config.BeansConfiguration;

public class DemoClass {
    private static final Logger logger = Logger.getLogger(Demo.class);
    ApplicationContext appContext;

    public void createScrumTeam() {
        try {
            logger.info("In createScrumTeam() : ");
// Créer un Spring Context Container (IoC) :
            appContext = new AnnotationConfigApplicationContext(BeansConfiguration.class);
// Récupérer les beans
            ScrumTeam bean1 = (ScrumTeam) appContext.getBean("scrum-team");
            TeamMember bean2 = (TeamMember) appContext.getBean("dev-salma");
            bean1.setDevelopper(bean2);
            logger.debug("developper : " + bean1.getDevelopper().getName());
            TeamMember bean3 = (TeamMember) appContext.getBean("po-rihab");
            bean1.setProductOwner(bean3);
            logger.debug("product-owner : " + bean1.getProductOwner().getName());
            logger.debug("Out of createScrumTeam() : ");
        } catch (Exception e) {
            logger.error("Error : " + e);
        } finally {
        }
    }
}
