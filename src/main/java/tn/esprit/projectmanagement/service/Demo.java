package tn.esprit.projectmanagement.service;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo {
    private static final Logger logger = Logger.getLogger(Demo.class);

    public void createScrumTeam() {
        ApplicationContext appContext;
        try {
            logger.info("In createScrumTeam () : ");
            // Chargmeent du Conteneur Spring IoC :
            appContext = new ClassPathXmlApplicationContext("tp3-beans.xml");
// Récupération du Bean ScrumTeam :
//ScrumTeam scrumTeam = new ScrumTeam(); C'est fini cette façon de faire.
            ScrumTeam scrumTeam = (ScrumTeam) appContext.getBean("scrumTeam");
            logger.debug("developper : " + scrumTeam.getDevelopper().getName());
            logger.debug("scrumMaster : " + scrumTeam.getScrumMaster().getName());
            logger.debug("productOwner : " + scrumTeam.getProductOwner().getName());
            logger.info("Out of createScrumTeam (). ");
        } catch (Exception e) {
            logger.error("Error : " + e);
        } finally {
        }
    }
}
