package tn.esprit.projectmanagement.service;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import tn.esprit.projectmanagement.config.BeansConfigurationAnnotation;

public class DemoAnnotation {
    private static final Logger logger = Logger.getLogger(DemoAnnotation.class);
    ApplicationContext appContext;
    public void createScrumTeam () {
        try {
            logger.info("In createScrumTeam() : ");
// Créer un Spring Context Container (IoC) :
            appContext = new AnnotationConfigApplicationContext(BeansConfigurationAnnotation.class);
// Récupérer les beans
            ScrumTeamAnnotation bean1 = (ScrumTeamAnnotation) appContext.getBean("scrum-team");
            appContext.getBean("scrum-team");
            TeamMemberAnnotation bean2 = (TeamMemberAnnotation) appContext.getBean("dev-salma");
            bean1.setDevelopper(bean2);
            logger.debug("developper : " + bean1.getDevelopper().getName());
            TeamMemberAnnotation bean3 =(TeamMemberAnnotation) appContext.getBean("po-rihab");
            bean1.setProductOwner(bean3);
            logger.debug("product-owner : " + bean1.getProductOwner().getName());
            logger.debug("Out of createScrumTeam() : ");
        } catch (Exception e) {
            logger.error("Error : " + e);
        } finally {
        }

    }
}
