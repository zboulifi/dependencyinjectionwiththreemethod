package tn.esprit.projectmanagement.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
  
@Configuration
public class BeansConfiguration {
  
	@Bean(name="dev-salma")
	public TeamMember getTeamMemberOne()
	{
		TeamMember ret = new TeamMember();
		ret.setName("Salma LANDOULSI");
		ret.setRole("dev");
		return ret;
	}
 
	@Bean(name="po-rihab")
	public TeamMember getTeamMemberTwo() 
	{ 
		TeamMember ret = new TeamMember(); 
		ret.setName("Rihab IDOUDI"); 
		ret.setRole("po");   
		return ret; 
	} 
   
} 
 
 